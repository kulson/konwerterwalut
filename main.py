import sys
import requests
from PySide6.QtWidgets import (QComboBox, QApplication, QLabel, QDoubleSpinBox, QMainWindow, QPushButton, QVBoxLayout)

currencies = ['AED' ,'AFN' ,'ALL' ,'AMD' ,'ANG' ,'AOA' ,'ARS' ,'AUD' ,'AWG' ,'AZN' ,'BAM' ,'BBD' ,'BDT' ,'BGN' ,'BHD' ,
              'BIF' ,'BMD' ,'BND' ,'BOB' ,'BRL' ,'BSD' ,'BTC' ,'BTN' ,'BWP' ,'BYN' ,'BZD' ,'CAD' ,'CDF' ,'CHF' ,'CLF' ,
              'CLP' ,'CNH' ,'CNY' ,'COP' ,'CRC' ,'CUC' ,'CUP' ,'CVE' ,'CZK' ,'DJF' ,'DKK' ,'DOP' ,'DZD' ,'EGP' ,'ERN' ,
              'ETB' ,'EUR' ,'FJD' ,'FKP' ,'GBP' ,'GEL' ,'GGP' ,'GHS' ,'GIP' ,'GMD' ,'GNF' ,'GTQ' ,'GYD' ,'HKD' ,'HNL' ,
              'HRK' ,'HTG' ,'HUF' ,'IDR' ,'ILS' ,'IMP' ,'INR' ,'IQD' ,'IRR' ,'ISK' ,'JEP' ,'JMD' ,'JOD' ,'JPY' ,'KES' ,
              'KGS' ,'KHR' ,'KMF' ,'KPW' ,'KRW' ,'KWD' ,'KYD' ,'KZT' ,'LAK' ,'LBP' ,'LKR' ,'LRD' ,'LSL' ,'LYD' ,'MAD' ,
              'MDL' ,'MGA' ,'MKD' ,'MMK' ,'MNT' ,'MOP' ,'MRU' ,'MUR' ,'MVR' ,'MWK' ,'MXN' ,'MYR' ,'MZN' ,'NAD' ,'NGN' ,
              'NIO' ,'NOK' ,'NPR' ,'NZD' ,'OMR' ,'PAB' ,'PEN' ,'PGK' ,'PHP' ,'PKR' ,'PLN' ,'PYG' ,'QAR' ,'RON' ,'RSD' ,
              'RUB' ,'RWF' ,'SAR' ,'SBD' ,'SCR' ,'SDG' ,'SEK' ,'SGD' ,'SHP' ,'SLL' ,'SOS' ,'SRD' ,'SSP' ,'STD' ,'STN' ,
              'SVC' ,'SYP' ,'SZL' ,'THB' ,'TJS' ,'TMT' ,'TND' ,'TOP' ,'TRY' ,'TTD' ,'TWD' ,'TZS' ,'UAH' ,'UGX' ,'USD' ,
              'UYU' ,'UZS' ,'VES' ,'VND' ,'VUV' ,'WST' ,'XAF' ,'XAG' ,'XAU' ,'XCD' ,'XDR' ,'XOF' ,'XPD' ,'XPF' ,'XPT' ,
              'YER' ,'ZAR' ,'ZMW' ,'ZWL']


API_KEY = "5113e500a6074adf9b30ac4013b0f57c"
BASE = "USD"

URL = f"https://openexchangerates.org/api/latest.json?app_id={API_KEY}&base={BASE}"

class Form(QMainWindow):

    rates = dict()

    def __init__(self, parent=None):
        response = requests.get(URL).json()
        self.rates = response["rates"]
        super(Form, self).__init__(parent)

        ### init  

        self.lbl = QLabel("Currency conversion", self)
        self.fromComboBox = QComboBox(self)
        self.fromComboBox.addItems(currencies)
        self.button = QPushButton("Swap", self)
        self.toComboBox = QComboBox(self)
        self.toComboBox.addItems(currencies)
        self.fromSpinBox = QDoubleSpinBox(self)
        self.fromSpinBox.setRange(0.01, 1_000_000.00)
        self.fromSpinBox.setValue(1.00)
        self.toSpinBox= QDoubleSpinBox(self)
        self.toSpinBox.setRange(0.01, 1_000_000.00)
        self.toSpinBox.setValue(1.00)
        
        ### positioning of elements in GUI design ###

        self.lbl.move(20,25)
        self.lbl.adjustSize()
        self.button.move(135, 50)
        self.fromComboBox.move(20, 50)
        self.toComboBox.move(250, 50)
        self.fromSpinBox.move(20, 90)
        self.toSpinBox.move(250, 90)

        self.setGeometry(200, 30, 540, 360)
        self.setWindowTitle("Currency Converter") 

        ### update gui

        self.button.clicked.connect(self.boxChange)
        self.fromComboBox.currentIndexChanged.connect(self.uiUpdate)
        self.toComboBox.currentIndexChanged.connect(self.uiUpdate)
        self.fromSpinBox.valueChanged.connect(self.uiUpdate)
        self.toSpinBox.valueChanged.connect(self.uiUpdate2)

        ### setting layout

        layout = QVBoxLayout()
        self.setLayout(layout)

    ### update events


    def boxChange(self):
        from_currency = self.fromComboBox.currentText()
        to_currency = self.toComboBox.currentText()
        self.fromComboBox.setCurrentText(to_currency)
        self.toComboBox.setCurrentText(from_currency)


    def uiUpdate(self):
        from_currency = self.fromComboBox.currentText()
        to_currency = self.toComboBox.currentText()
        amount = self.fromSpinBox.value()
        if from_currency != 'USD':
            amount = amount / self.rates[from_currency]
        amount = round(amount * self.rates[to_currency], 2)
        self.toSpinBox.setValue(amount)


    def uiUpdate2(self):
        from_currency = self.toComboBox.currentText()
        to_currency = self.fromComboBox.currentText()
        amount = self.toSpinBox.value()
        if from_currency != 'USD':
            amount = amount / self.rates[from_currency]
        amount = round(amount * self.rates[to_currency], 2)
        self.fromSpinBox.setValue(amount)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    form.show()
    ### Run loop 
    sys.exit(app.exec())

